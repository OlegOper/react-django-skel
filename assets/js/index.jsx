import React from 'react'
import ReactDOM from 'react-dom'
import RaisedButton from 'material-ui/RaisedButton';
import {deepOrange500} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import "css/style.css"

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 200,
  },
};

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

function Square(props) {
	return (
		<button className="square" onClick={() => props.onClick()}>
			{props.value}
		</button>
	);
}

class Board extends React.Component {
	constructor(props){
		super(props);
	}
	renderSquare(i) {
		return <Square value={this.props.squares[i]} onClick={() => this.props.onClick(i)} />;
	}
	render() {
		return (
			<div>
				<div className="board-row">
					{this.renderSquare(0)}
					{this.renderSquare(1)}
					{this.renderSquare(2)}
				</div>
				<div className="board-row">
					{this.renderSquare(3)}
					{this.renderSquare(4)}
					{this.renderSquare(5)}
				</div>
				<div className="board-row">
					{this.renderSquare(6)}
					{this.renderSquare(7)}
					{this.renderSquare(8)}
				</div>
			</div>
		);
	}
}

class Game extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			history: [{
				squares:Array(9).fill(null),
			}],
			stepNumber: 0,
			next: 'X',
		};
	}
	newGame(){
		let newState = {
			history: [{
				squares:Array(9).fill(null),
			}],
			stepNumber: 0,
			next: 'X',
		};
		this.setState(newState);
	}
	handleClick(i){
		const history = this.state.history.slice(0,this.state.stepNumber+1);
		const current = history[history.length-1];
		const squares = current.squares.slice();
		if(calculateWinner(squares) || squares[i])
			return;
		squares[i] = this.state.next;
		let next = this.state.next=='X'?'O':'X';
		this.setState({
			history: history.concat([{
				squares:squares
			}]),
			stepNumber: history.length,
			next:next,
		});
	}
	jumpTo(step){
		this.setState({
			stepNumber: step,
			next: (step % 2 == 0) ? 'X':'O',
		});
	}
	render() {
		const history = this.state.history;
		const current = history[this.state.stepNumber];
		const moves = history.map((squares, index) => {
			const desc = (index!=0)?'Move #' + index:'Game start';
			return (
				<li key={index}>
					<a href="#" onClick={() => this.jumpTo(index)}>{desc}</a>
				</li>
			);
		});
		const winner = calculateWinner(current.squares);
		let status;
		if (winner) {
			status = 'Winner: ' + winner;
		} else {
			status = 'Next player: '+this.state.next;
		}
		return (
		<MuiThemeProvider muiTheme={muiTheme}>
			<div className="game">
				<div className="game-board">
					<Board squares={current.squares} onClick = {(i) => this.handleClick(i)}/>
				</div>
				<div className="game-info">
					<RaisedButton label="Default" onClick={ () => this.newGame()} />
					<div>{status}</div>
					<ol>{moves}</ol>
				</div>
			</div>
		</MuiThemeProvider>
		);
	}
}

// ========================================

ReactDOM.render(
	<Game />,
	document.getElementById('container')
);

function calculateWinner(squares) {
	const lines = [
		[0, 1, 2],
		[3, 4, 5],
		[6, 7, 8],
		[0, 3, 6],
		[1, 4, 7],
		[2, 5, 8],
		[0, 4, 8],
		[2, 4, 6],
	];
	for (let i = 0; i < lines.length; i++) {
		const [a, b, c] = lines[i];
		if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
			return squares[a];
		}
	}
	return null;
}
