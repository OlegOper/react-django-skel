var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

module.exports = {
  context: __dirname,

  entry: [
	//'webpack-dev-server/client?http://localhost:3000',
	//'webpack/hot/only-dev-server',
	'./assets/js/index', // entry point of our app. assets/js/index.js should require other js modules and dependencies it needs
  ],
  output: {
      path: path.resolve('./assets/bundles/'),
      filename: "[name]-[hash].js",
	  //publicPath: 'http://localhost:3000/assets/bundles/',
  },

  plugins: [
	//new webpack.HotModuleReplacementPlugin(),
	new webpack.NoErrorsPlugin(),
    new BundleTracker({filename: './webpack-stats.json'}),
  ],

  module: {
    loaders: [
      { test: /\.jsx$/, exclude: /node_modules/, loader: 'babel-loader', query:{ presets: ['es2015','react']}}, // to transform JSX into JS
	  { test: /\.css$/, loader: "style-loader!css-loader" }
    ],
  },

  resolve: {
	root: [ path.resolve('./assets'), path.resolve('./'), ],
    modulesDirectories: ['node_modules', 'bower_components'],
    extensions: ['', '.js', '.jsx', '.css']
  },
}
