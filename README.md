# Django with ReactJS Integration #

This is the barebone project following facebook's tic-tac-toe tutorial [https://facebook.github.io/react/tutorial/tutorial.html](https://facebook.github.io/react/tutorial/tutorial.html) 

One can use this for getting started with ReactJS with Material UI using Django Rest Framework with Webpack loader.

### Installation ###

Ensure you have `virtualenv`, `npm` and `pip` installed and then do

```
cd react-django-skel 
virtualenv env
chmod +x install_*
./install_pip_requirements.sh
./install_npm_requirements.sh
```

### Run ###

#### Run without Hot Reloading ####
In one terminal:
```
cd react-django-skel
./node_modules/.bin/webpack --config webpack.config.js --watch
```

In another terminal:
```
cd react-django-skel
python manage.py runserver
```

#### To enable Hot Reloading ####
[http://owaislone.org/blog/webpack-plus-reactjs-and-django/](http://owaislone.org/blog/webpack-plus-reactjs-and-django/)

#### A Short Note on the `history.map(()=>{})` Syntax ####

##### Arrow Functions #####
[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)

##### Array.prototype.map() #####
[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)
